-- ОЧИСТКА
delete from [dbo].[Dogovory_spr];
delete from [dbo].[Dogovory];
delete from [dbo].[Facultety];
delete from [dbo].[Otdelenia];
delete from [dbo].[Specialnosty];
delete from [dbo].[SrokyObuchenia];
delete from [dbo].[Nachislenia];
delete from [dbo].[StudentIstor];
delete from [dbo].[Studenty];

-- ЧИСТИМ ТОЛЬКО ДОХОДЫ
delete from [dbo].[RegistrDohody];
update [dbo].[Nachislenia] set VkluchitDohod = 0;
update [dbo].[StudentIstor] set VkluchitDohod = 0;

-- Заполнение курсов и семестров
update [dbo].[StudentIstor] set Semestr = 0, Kurs = 0 
where Sostoyanie in ('АкадемическийОтпуск', 'Восстановление', 'Отчисление');

update [dbo].[StudentIstor]
set Semestr = dbo.get_last_semestr(StudentId, DataSost)
where isnull(Semestr,0)<1;

-- Расчет сроков обучения

update SrokyObuchenia set SrokSemestr = SrokLet*2 + Round((SrokMes+0.01)/6,0);
update Specialnosty set SrokObucheniaLet = 0, SrokObucheniaMes = 0, SrokObuchSemestr = 0;

--
update s 
set SrokObuchSemestr = so.SrokSemestr, 
SrokObucheniaLet = so.SrokLet, 
SrokObucheniaMes = so.SrokMes
from Specialnosty s, SrokyObuchenia so
where s.id = so.SpecialnostId;

--======== НАЧАЛО ОТСЮДА ПРИ ФОРМИРОВАНИИ ДОХОДОВ =========---

-- Включение начислений

select distinct n.Id, n.StudentId, i1.SpecialnostId, n.GodNachisl, n.Semestr, i1.Semestr as SemestrZachisl, n.Summa
into ##t1
from [dbo].[Nachislenia] n
left outer join [dbo].[StudentIstor] i1 on (
n.StudentId = i1.StudentId and rtrim(i1.Sostoyanie) in ('ПриемНаПервыйКурс', 'ПереводНаСледующийКурс', 'Перевод')
and not exists (select 1 from [dbo].[StudentIstor] i0 where i0.StudentId = i1.StudentId and i0.DataSost<i1.DataSost)
)
where isnull(n.VkluchitDohod,0) = 0 
and n.Summa<>0
and rtrim(isnull(n.FormaObuch,''))<>'Целевая';

insert into [dbo].[RegistrDohody] (StudentId, SpecialnostId, GodRascheta, SemestrRascheta, SemestrZachisl, SummaNach)
select StudentId, SpecialnostId, GodNachisl, Semestr, SemestrZachisl, sum(Summa) from ##t1
group by StudentId, SpecialnostId, GodNachisl, Semestr, SemestrZachisl;

update [dbo].[Nachislenia] set VkluchitDohod = 1 
where [dbo].[Nachislenia].id in (select id from ##t1);

drop table ##t1;

--

-- Заполнение прерываний и восстановлений обучения

select distinct n.StudentId,  i1.SpecialnostId, year(n.DataSost) as God, n.Semestr, n.Sostoyanie, n.id, n.DokVid, n.DokNomer, n.DokData
into ##t2 
from [dbo].[StudentIstor] n
left outer join [dbo].[StudentIstor] i1 on (
n.StudentId = i1.StudentId and rtrim(i1.Sostoyanie) in ('ПриемНаПервыйКурс', 'ПереводНаСледующийКурс', 'Перевод')
and not exists (select 1 from [dbo].[StudentIstor] i0 where i0.StudentId = i1.StudentId and i0.DataSost<i1.DataSost)
)
where n.Sostoyanie in ('АкадемическийОтпуск', 'Восстановление', 'Отчисление')
and isnull(n.VkluchitDohod,0) = 0 
and exists (
select 1 from [dbo].[Nachislenia] n1 where n1.StudentId = n.StudentId and n1.Summa<>0
);

insert into [dbo].[RegistrDohody] (StudentId, SpecialnostId, GodRascheta, SemestrRascheta, TypIstor, IstorId, IstorDokVid, IstorDokNomer, IstorDokData)
select StudentId, SpecialnostId, God, Semestr,  Sostoyanie, id, DokVid, DokNomer, DokData from ##t2;

update [dbo].[StudentIstor] set VkluchitDohod = 1 
where id in (select id from ##t2);

drop table ##t2;


-- Заполнение предыдущих Состояний
DECLARE @Id int, @StudentId nchar(20), @IstorId nchar(40), @TypIstor varchar(30), @OrderNum int;
DECLARE @OldId int, @OldStudentId nchar(20), @OldIstorId nchar(40), @OldTypIstor varchar(30);

DECLARE cRegistrDohody CURSOR FOR   
select Id, StudentId, IstorId, TypIstor
from [dbo].[RegistrDohody] 
order by StudentId, GodRascheta, IstorDokData, SemestrRascheta; 
		
open cRegistrDohody;
		
fetch next from cRegistrDohody into 
@Id, @StudentId, @IstorId, @TypIstor;

set @OldId = @Id;
set @OldStudentId = @StudentId;
set @OldIstorId = @IstorId;
set @OldTypIstor = @TypIstor;
set @OrderNum = 1;

while @@fetch_status = 0  
begin 
			
	if @StudentId = @OldStudentId
	begin

		update [dbo].[RegistrDohody] set 
		PredId = @OldId, 
		PredIstorId = @OldIstorId,
		PredTypIstor = @OldTypIstor, 
		OrderNum = @OrderNum
		where Id = @Id;

	end 
	else
	begin

	  set @OldId = null;
	  set @OldTypIstor = null;
	  set @OldIstorId = null;
	  set @OrderNum = 0;

	end;

	set @OldId = @Id;
	set @OldStudentId = @StudentId;
	set @OldIstorId = isnull(@IstorId, @OldIstorId);
	set @OldTypIstor = isnull(@TypIstor, @OldTypIstor);
	set @OrderNum = @OrderNum + 1;

	fetch next from cRegistrDohody into 
	@Id, @StudentId, @IstorId, @TypIstor;

end; 

close cRegistrDohody;
deallocate cRegistrDohody;

-- Заполнение сроков обучения

update d 
set SrokObuch =  sp.SrokObuchSemestr, 
SrokObuchMes = sp.SrokObucheniaLet*12+sp.SrokObucheniaMes 
from [dbo].[RegistrDohody] d
inner join [dbo].[StudentIstor] i on (i.StudentId = d.StudentId and i.Sostoyanie = 'ПриемНаПервыйКурс')
inner join [dbo].[Specialnosty] sp on (sp.id = i.SpecialnostId);

-- Срок обучения сбрасывается в случае прекращения обучения
-- 21 01 2024 - уберем это и сравним результат.
update [dbo].[RegistrDohody] 
set SrokObuch = SemestrRascheta, 
SrokObuchMes = SemestrRascheta*6 
where TypIstor in ('АкадемическийОтпуск', 'Отчисление');

-- Остатки обучения
update [dbo].[RegistrDohody] 
set OstatokObuch = SrokObuch-SemestrRascheta+1, 
OstatokObuchMes = SrokObuchMes-SemestrRascheta*6+6;

update [dbo].[RegistrDohody] set OstatokObuch = 1
where OstatokObuch<1;

update [dbo].[RegistrDohody] 
set OstatokObuchMes = SrokObuchMes - (SrokObuch-1)*6 -- срок обучения в месяцах последнего семестра
where OstatokObuch=1;

-- ОТСЮДА НАЧНЕМ ВСЕ ПО ДРУГОМУ ---
-- все стер. как было - смотреть в старом файле

--1) Для НЕисторических записей заполняем "Предыдущий семестр расчета" и "Предыдущий срок обучения"
-- учитываем перескакивание начислений, а также пропуски, поэтому семестр и срок заполняем не только по предыдущему ID, но и по меньшему семестру.

update d set 
PredSemestrRascheta = 0,
PredSrokObuch = 0,
PredOstatokObuch = 0
from [dbo].[RegistrDohody] d;


update d set 
PredSemestrRascheta = d1.SemestrRascheta,
PredSrokObuch = d1.SrokObuch,
PredOstatokObuch = d1.OstatokObuch
from [dbo].[RegistrDohody] d
inner join [dbo].[RegistrDohody] d1 on (
d1.StudentId = d.StudentId and d1.TypIstor is null and isnull(d1.OrderNum,0)<isnull(d.OrderNum,0) and d1.SemestrRascheta<d.SemestrRascheta
and not exists (
     select 1 from [dbo].[RegistrDohody] d2 
	 where d2.StudentId = d.StudentId 
	 and isnull(d2.OrderNum,0)<isnull(d.OrderNum,0) 
	 and d2.SemestrRascheta<d.SemestrRascheta 
	 and d2.SemestrRascheta>d1.SemestrRascheta
  )
)
where d.TypIstor is null;

--2) Для НЕисторических записей заполняем "Предыдущую сумму начислений
-- делается отдельным запросом потому что нам нужна сумма

update d set PredSummaNach = 0 from [dbo].[RegistrDohody] d;

update d
set PredSummaNach=
(
select sum(d1.SummaNach) from [dbo].[RegistrDohody] d1 where d1.StudentId = d.StudentId and d1.TypIstor is null and isnull(d1.OrderNum,0)<isnull(d.OrderNum,0) and d1.SemestrRascheta<d.SemestrRascheta
and not exists (
     select 1 from [dbo].[RegistrDohody] d2 
	 where d2.StudentId = d.StudentId 
	 and isnull(d2.OrderNum,0)<isnull(d.OrderNum,0) 
	 and d2.SemestrRascheta<d.SemestrRascheta 
	 and d2.SemestrRascheta>d1.SemestrRascheta
  )
)
from [dbo].[RegistrDohody] d
where d.TypIstor is null;

-- РАСЧЕТ ДОХОДОВ

update [dbo].[RegistrDohody] set SummaDohod = 0;

-- Если такой семестр уже встречался то обнуляем предыдущую сумму начислений, потому что мы должны ДОНАЧИСЛИТЬ всю сумму.
update d
set PredSummaNach = 0 
from [dbo].[RegistrDohody] d
where exists (
  select 1 from [dbo].[RegistrDohody] d1 
  where d1.StudentId = d.StudentId
  and d1.SemestrRascheta = d.SemestrRascheta
  and isnull(d1.OrderNum,0) < isnull(d.OrderNum,0) 
)
and isnull(TypIstor,'') = '';

--1) Расчет доходов для Неисторических записей
update [dbo].[RegistrDohody] 
set SummaDohod = Isnull(SummaNach,0)*Isnull(OstatokObuch,0) - isnull(PredSummaNach,0)*(isnull(PredOstatokObuch,0)-1)
where TypIstor is null;

-- перерасчет доходов для Отчислений После Отчислений
delete from [dbo].[RegistrPererasch];

insert into [dbo].[RegistrPererasch] (DohodIstorId, DohodNachId, SumPererasch)
select 
(select d1.id from [dbo].[RegistrDohody] d1 where d1.IstorId = d.PredIstorId),
id, 
Isnull(SummaNach,0)*Isnull(OstatokObuch,0) - isnull(PredSummaNach,0)*(isnull(PredOstatokObuch,0)-1) - isnull(d.SummaNach,0)
from [dbo].[RegistrDohody] d
where d.PredTypIstor in ('Отчисление','АкадемическийОтпуск');

-- для отчисления и Академа сумму снятия считаем как Предыдущий доход - Сумма фактических начислений
update d
set SummaDohod=
(select sum(d1.SummaNach) from [dbo].[RegistrDohody] d1 where d1.StudentId = d.StudentId and isnull(d1.OrderNum,0) < isnull(d.OrderNum,0) and isnull(d1.TypIstor,'')='')
-(select sum(d0.SummaDohod) from [dbo].[RegistrDohody] d0 where d0.StudentId = d.StudentId and isnull(d0.OrderNum,0) < isnull(d.OrderNum,0)) 
from [dbo].[RegistrDohody] d
where d.TypIstor in ('Отчисление','АкадемическийОтпуск');

-- Если отчисление после Академа или наоборот , то сумму обнуляем и удаляем перерасчеты
delete from [dbo].[RegistrPererasch] where DohodIstorId in
(select d.id from [dbo].[RegistrDohody] d where d.PredTypIstor in ('Отчисление','АкадемическийОтпуск') and d.TypIstor in ('Отчисление','АкадемическийОтпуск'));

update [dbo].[RegistrDohody] 
set SummaDohod = 0
where PredTypIstor in ('Отчисление','АкадемическийОтпуск') and TypIstor in ('Отчисление','АкадемическийОтпуск');


-- для восстановления считаем сумму последнего отчисления + все перерасчеты
update d
set SummaDohod=
-(
	select sum(s1.sumold)
	from 
	(
	select d0.SummaDohod - (select sum(p.SumPererasch) from [dbo].[RegistrPererasch] p where p.DohodIstorId = d0.id) as sumold
	from [dbo].[RegistrDohody] d0 
	where d0.StudentId = d.StudentId 
	and isnull(d0.OrderNum,0) < isnull(d.OrderNum,0) 
	and d0.TypIstor in ('Отчисление','АкадемическийОтпуск')
	and not exists (
		select 1 from [dbo].[RegistrDohody] d1 
		where d1.StudentId = d0.StudentId 
		and isnull(d1.OrderNum,0) > isnull(d0.OrderNum,0) 
		and isnull(d1.OrderNum,0) < isnull(d.OrderNum,0) 
		and d1.TypIstor in ('Отчисление','АкадемическийОтпуск')
	  )
	) s1
), 
SummaNach = 0
from [dbo].[RegistrDohody] d
where d.TypIstor = 'Восстановление';

--
-- ОТЧЕТ --
update Studenty set Naim = rtrim(Naim);
update Facultety set Naim = rtrim(Naim);
update Specialnosty set Naim = rtrim(Naim);
update Otdelenia set Naim = rtrim(Naim);

update [dbo].[RegistrDohody] set [SummaPererasch] = (select -sum(p.SumPererasch) from RegistrPererasch p where p.DohodNachId = [dbo].[RegistrDohody].id);
update [dbo].[RegistrDohody] set [SummaDohodItog] = isnull(SummaDohod,0)+isnull(SummaPererasch,0);

-- Разнесение по годам (текущий год и будущие года)

declare @Id int, 
@StudentId nchar(20), @OldStudentId nchar(20), 
@SemestrRascheta int, 
@SrokObuchMes int, @OstObuchMes int, 
@SrokObuch int, @OstatokObuch int, 
@SummaDohodItog decimal(12,4),
@KolMesTekSemestr int, @KolMesPredSemestr int, @OstObuchBudLetMes int, 
@SumMesSpis decimal(12,4), @SumMesSpisPred decimal(12,4); 


DECLARE cRegistrDohody CURSOR FOR   
select Id, StudentId, SemestrRascheta, OstatokObuchMes, SummaDohodItog
from [dbo].[RegistrDohody] 
where SrokObuch>0
order by StudentId, OrderNum;

open cRegistrDohody;
		
fetch next from cRegistrDohody into 
@Id, @StudentId, @SemestrRascheta, @OstObuchMes, @SummaDohodItog;

set @SumMesSpisPred = 0;
set @OstObuchBudLetMes = @OstObuchMes;
set @OldStudentId = @StudentId;

while @@fetch_status = 0  
begin 

	if @StudentId <> @OldStudentId
	begin
	  set @SumMesSpisPred = 0;
	  set @OstObuchBudLetMes = @OstObuchMes;
	  set @OldStudentId = @StudentId;
	end;

	if @SemestrRascheta%2>0
	begin
		set @KolMesTekSemestr = 4;
		set @KolMesPredSemestr = 0;
	end else
	begin
		set @KolMesTekSemestr = 6;
		set @KolMesPredSemestr = 2;
	end;

	if @KolMesTekSemestr>@OstObuchMes
	set @KolMesTekSemestr = @OstObuchMes;

	set @SumMesSpis = @SummaDohodItog/@OstObuchMes;
	set @OstObuchBudLetMes = @OstObuchMes - @KolMesTekSemestr;
	if @OstObuchBudLetMes<0 set @OstObuchBudLetMes = 0;
	
	update [dbo].[RegistrDohody]
	set SumMesSpis = @SumMesSpis, 
	OstObuchBudLetMes = @OstObuchBudLetMes, 
	KolMesTekSemestr = @KolMesTekSemestr, 
	KolMesPredSemestr = @KolMesPredSemestr, 
	NachislDohTekGod = @SumMesSpis*@KolMesTekSemestr+@SumMesSpisPred*@KolMesPredSemestr, 
	NachislDohBudLet = @SumMesSpis*@OstObuchBudLetMes-@SumMesSpisPred*@KolMesPredSemestr
	where id = @id;

	set @SumMesSpisPred = @SumMesSpis;

	fetch next from cRegistrDohody into 
	@Id, @StudentId, @SemestrRascheta, @OstObuchMes, @SummaDohodItog;

end;

close cRegistrDohody;
deallocate cRegistrDohody;


-- распределение по годам + 5 лет - каждый год индивидуально

--Начало
delete from RegistrDohodyByYears;

declare @Id bigint, @StudentId nchar(20), @GodRascheta int, @SemestrRascheta int, 
@SumMesSpis decimal(12,2), @KolMes int, @OstatokObuchMes int;

DECLARE cRegistrDohody CURSOR FOR   
select Id, StudentId, GodRascheta, SemestrRascheta, SumMesSpis, OstatokObuchMes
from [dbo].[RegistrDohody] d
where SrokObuch>0
and exists (
 select 1 from [dbo].[RegistrDohody] d2 where d2.StudentId = d.StudentId and d2.GodRascheta = 2023
)
order by StudentId, OrderNum;

open cRegistrDohody;
		
fetch next from cRegistrDohody into 
@Id, @StudentId, @GodRascheta, @SemestrRascheta, @SumMesSpis, @OstatokObuchMes;

while @@fetch_status = 0  
begin 

	if @SemestrRascheta%2>0
	begin
		set @KolMes = 4;
	end else
	begin
		set @KolMes = 8;
	end;

	if @KolMes>@OstatokObuchMes
	set @KolMes=@OstatokObuchMes;

	insert into RegistrDohodyByYears (DohodyId, StudentId, God, SumMesSpis, KolMes, NachislDoh, SemestrRascheta, OstatokObuchMes)
	values (@id, @StudentId, @GodRascheta, @SumMesSpis, @KolMes, @SumMesSpis*@KolMes, @SemestrRascheta, @OstatokObuchMes);

	set @OstatokObuchMes = @OstatokObuchMes - @KolMes;

	WHILE @OstatokObuchMes>0
	BEGIN

		set @SemestrRascheta = @SemestrRascheta+1;

		if @SemestrRascheta%2>0
		begin
			set @KolMes = 4;
		end else
		begin
			set @GodRascheta = @GodRascheta +1;
			set @KolMes = 8;
		end;

		if @KolMes>@OstatokObuchMes
		set @KolMes = @OstatokObuchMes;

		insert into RegistrDohodyByYears (DohodyId, StudentId, God, SumMesSpis, KolMes, NachislDoh, SemestrRascheta, OstatokObuchMes)
		values (@id, @StudentId, @GodRascheta, @SumMesSpis, @KolMes, @SumMesSpis*@KolMes, @SemestrRascheta, @OstatokObuchMes);

		set @OstatokObuchMes = @OstatokObuchMes - @KolMes;

	END

	fetch next from cRegistrDohody into 
	@Id, @StudentId, @GodRascheta, @SemestrRascheta, @SumMesSpis, @OstatokObuchMes;

end;

close cRegistrDohody;
deallocate cRegistrDohody;



--

select 
--o.MestoObuch, 
d.id, 
d.StudentId,
o.Naim as Otdelenie, 
f.Naim as Facultet,  
''''+sp.Kod as spKod,
sp.Naim as Specialnost, 
s.Naim as Student,  
d.SemestrZachisl, 
--d.PredGodRascheta, 
d.PredSemestrRascheta, 
d.PredOstatokObuch,
d.PredSummaNach, 
d.GodRascheta, 
d.SemestrRascheta, 
d.SummaNach, 
--d.PredTypIstor, 
d.TypIstor, 
d.SrokObuch, 
d.OstatokObuch,
d.OstatokObuchMes, 
d.SummaDohod, 
d.SummaPererasch,
d.SummaDohodItog, 
d.SumMesSpis,
d.KolMesTekSemestr, 
d.KolMesPredSemestr, 
d.OstObuchBudLetMes,
d.NachislDohTekGod,
d.NachislDohBudLet,
(select sum(dy.NachislDoh) from RegistrDohodyByYears dy where dy.God = 2018 and dy.DohodyId = d.id) d2018,
(select sum(dy.NachislDoh) from RegistrDohodyByYears dy where dy.God = 2019 and dy.DohodyId = d.id) d2019,
(select sum(dy.NachislDoh) from RegistrDohodyByYears dy where dy.God = 2020 and dy.DohodyId = d.id) d2020,
(select sum(dy.NachislDoh) from RegistrDohodyByYears dy where dy.God = 2021 and dy.DohodyId = d.id) d2021,
(select sum(dy.NachislDoh) from RegistrDohodyByYears dy where dy.God = 2022 and dy.DohodyId = d.id) d2022,
(select sum(dy.NachislDoh) from RegistrDohodyByYears dy where dy.God = 2023 and dy.DohodyId = d.id) d2023,
(select sum(dy.NachislDoh) from RegistrDohodyByYears dy where dy.God = 2024 and dy.DohodyId = d.id) d2024,
(select sum(dy.NachislDoh) from RegistrDohodyByYears dy where dy.God = 2025 and dy.DohodyId = d.id) d2025,
(select sum(dy.NachislDoh) from RegistrDohodyByYears dy where dy.God = 2026 and dy.DohodyId = d.id) d2026,
(select sum(dy.NachislDoh) from RegistrDohodyByYears dy where dy.God = 2027 and dy.DohodyId = d.id) d2027,
(select sum(dy.NachislDoh) from RegistrDohodyByYears dy where dy.God = 2028 and dy.DohodyId = d.id) d2028,
(select sum(dy.NachislDoh) from RegistrDohodyByYears dy where dy.God = 2029 and dy.DohodyId = d.id) d2029
from 
[dbo].[RegistrDohody] d, 
[dbo].[Studenty] s, 
[dbo].[StudentIstor] i,
[dbo].[Specialnosty] sp, 
[dbo].[Facultety] f, 
[dbo].[Otdelenia] o
where 
d.StudentId = s.id 
and i.StudentId = s.id
and i.Sostoyanie = 'ПриемНаПервыйКурс'
and i.SpecialnostId = sp.id
and sp.FacultetId = f.id
and f.OtdelenieId = o.id
--and s.Naim = 'Агапова Мирослава Викторовна'
and exists (
 select 1 from [dbo].[RegistrDohody] d2 where d2.StudentId = d.StudentId and d2.GodRascheta = 2023
)
-- условие фильтрации
and (
	(
		not (o.Naim = 'Дневное отделение' and f.Naim = 'CИРОТЫ')
		and not (o.Naim = 'Дневное отделение' and f.Naim = 'Бакалавры')
		and not (o.Naim = 'Дневное отделение' and f.Naim = 'Магистратура')
		and not (o.Naim = 'Дневное отделение' and f.Naim = 'Заочный факультет')
		and not (o.Naim = 'Дневное отделение' and f.Naim = 'Общежитие')
		and not (o.Naim = 'Дневное отделение' and f.Naim = 'эРазное')
		and not (o.Naim = 'Дневное отделение' and f.Naim = 'АСПР')
		and not (o.Naim = 'Филиалы Очно-Заочная')
		and not (o.Naim = 'Филиалы дневное отделение')
		and not (o.Naim = 'Заочное отделение' and f.Naim <> 'Институт Интегрированных Форм Обучения' and sp.Naim <> 'АСПИРАНТЫ')
		and not (o.Naim = 'Заочное отделение' and sp.Naim = 'АСПИРАНТЫ')
	)
) 
order by s.Naim, d.StudentId, OrderNum;


